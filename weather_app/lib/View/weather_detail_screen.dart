import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:weather_app/Model/weather_provider.dart';


class WeatherDetailScreen extends StatefulWidget {
  const WeatherDetailScreen({super.key});

  @override
  State<WeatherDetailScreen> createState() => _WeatherDetailScreenState();
}

class _WeatherDetailScreenState extends State<WeatherDetailScreen> {

  @override
  void didChangeDependencies() {
    Provider.of<WeatherProvider>(context).getWeather('');
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {

    final weatherProvider=Provider.of<WeatherProvider>(context,listen:false);
    final weatherData=weatherProvider.weatherData;

    return Scaffold(
      backgroundColor:const Color(0xFF676BD0),
      appBar: AppBar(
        backgroundColor:const Color(0xFF676BD0) ,
      ),
      body:Container(
        height: MediaQuery.sizeOf(context).height,
        width: MediaQuery.sizeOf(context).width,
        alignment: Alignment.center,
        child: Padding(
          padding: const EdgeInsets.only(top:1),
          child: 
          (weatherData['name']!=null&&weatherData['main']['temp']!=null&&weatherData['weather'][0]['main']!=null)?
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text(
              weatherData['name'],
              style:const TextStyle(fontSize: 25,fontWeight: FontWeight.bold,color:Colors.white),
            ),
            const SizedBox(
              height: 5,
            ),
            Text(
              "${weatherData['main']['temp']}°C",
              style:const TextStyle(fontSize: 35,fontWeight: FontWeight.bold,color:Colors.white),
            ),
            const SizedBox(
              height: 5,
            ),
            Text(
              weatherData['weather'][0]['main'],
              style: GoogleFonts.poppins(fontSize: 25,fontWeight: FontWeight.bold,color:Colors.white),
            ),
            Container(
              height: MediaQuery.sizeOf(context).height*0.250,
              width:MediaQuery.sizeOf(context).height*0.300,
              decoration: BoxDecoration(
                //border: Border.all(),
                image:DecorationImage(fit:BoxFit.fill,image:NetworkImage("https://openweathermap.org/img/w/${weatherData['weather'][0]['icon']}.png",))
              ), 
              // child: Image.network(
              //   'https://openweathermap.org/img/w/${weatherData['weather'][0]['icon']}.png',
              //   fit:BoxFit.fill
              // ),
            ),
            const SizedBox(
              height: 20,
            ),
            Padding(
              padding: const EdgeInsets.only(left:20,right: 20),
              child: Container(
                height: MediaQuery.sizeOf(context).height*0.300,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: Colors.deepPurple
                ),
                child: Column(
                  children: [
                    const SizedBox(
                      height: 10,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        weatherColumn(iconColor:const Color.fromRGBO(255,255 , 255,1),iconData: Icons.wind_power, title: "Wind", value:"${weatherData['wind']['speed']} m/s" ),
                        weatherColumn(iconColor:const Color.fromARGB(242, 242, 122, 1),iconData: Icons.sunny, title: "Max Temp", value:"${weatherData['main']['temp_max']}°C" ),
                        weatherColumn(iconColor:const Color.fromRGBO(255,255 , 255,1),iconData: Icons.low_priority, title: "Min Temp", value:"${weatherData['main']['temp_min']}°C" ),
                      ],
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    const Padding(
                      padding: EdgeInsets.only(left:10,right: 10),
                      child: Divider(),
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        weatherColumn(iconColor:const Color.fromRGBO(255,255 , 255,1),iconData: Icons.air, title: "Pressure", value:"${weatherData['main']['pressure']} hpa" ),
                        weatherColumn(iconColor:const Color.fromRGBO(65,107,223,1),iconData: Icons.water_drop, title: "Humidity", value:"${weatherData['main']['humidity']}%" ),
                        weatherColumn(iconColor:const Color.fromRGBO(255,255 , 255,1),iconData: Icons.leaderboard, title: "Sea-level", value:"${weatherData['main']['sea_level']}m" ),
                      ],
                    )
                  ],
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Container(
              height: 40,
              width: 100,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: Colors.deepPurple
              ),
              child: GestureDetector(
                onTap: (){
                  weatherProvider.getWeather(weatherProvider.city);
                  didChangeDependencies();
                },
                child: const Text('Refresh',style: TextStyle(color:Colors.white,fontSize: 17,fontWeight:FontWeight.w500),),
              ),
            )
               
          ],
          ):const SizedBox(
            height: 50,
            width: 50,
            child:CircularProgressIndicator(
              strokeWidth:6,
              color: Colors.white,
            ),
          ),
        ),
      ),
    );
  }

  Column weatherColumn({required Color iconColor,required IconData? iconData,required String title,required String value}){
    return Column(
      children: [
        Text(
          title,
          style: const TextStyle(fontSize: 20,fontWeight: FontWeight.w600,color: Colors.white),
        ),
        Icon(iconData,color: iconColor,),
        Text(
          value,
          style: const TextStyle(fontSize: 20,fontWeight: FontWeight.w600,color: Colors.white),
        )
      ],
    );
  }
}