
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:weather_app/Model/weather_provider.dart';
import 'package:weather_app/View/weather_detail_screen.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {

  final TextEditingController searchCityController = TextEditingController();
  final Color mcolor = const Color(0xFF676BD0);

  void clearController(){
    searchCityController.clear();
  }


  @override
  Widget build(BuildContext context) {
    final weatherProvider=Provider.of<WeatherProvider>(context,listen: false);
    
    return  Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        title:const Text('Weather App',style: TextStyle(fontSize:25,fontWeight:FontWeight.w600,color: Colors.white),),
        backgroundColor: mcolor,
        centerTitle: true,
      ),
      body: Padding(
        padding: const EdgeInsets.all(20),
        child: Column(
          children: [
            const SizedBox(height: 20,),
            const Row(
              children: [
                SizedBox(width: 5,),
                Text(
                  "Enter City Name",
                  style: TextStyle(fontSize:20,fontWeight: FontWeight.normal,),
                ),
              ],
            ),
            const SizedBox(height: 5,),
            Container(
              //color: Colors.white,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: Colors.white
              ),
              child: TextFormField(
                controller: searchCityController,
                  decoration: InputDecoration(
                    prefixIcon: const Icon(Icons.location_city),
                    labelText: 'Search City',
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(20)
                    )
                  ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            Container(
              height: 40,
              width: 120,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: mcolor
              ),
              child: GestureDetector(
                onTap: () {
                  weatherProvider.getWeather(searchCityController.text);       
                  (searchCityController.text.isNotEmpty)?
                  Navigator.push(context, MaterialPageRoute(builder: (context)=>const WeatherDetailScreen(),))
                  :
                  ScaffoldMessenger.of(context).showSnackBar(
                    const SnackBar(
                      backgroundColor: Colors.red,
                      content: Text('Please Enter a City Name')),
                  );          
                  clearController();
                },
                child: const Text('Get Weather',style: TextStyle(color:Colors.white,fontSize: 16,fontWeight: FontWeight.bold),),
              ),
            ),
            const SizedBox(height: 10,),
            CarouselSlider(
            items: [
              weather('assets/images/sunny.png'),
              weather('assets/images/rainy.jpg'),
              weather('assets/images/cloudy.jpg'),

            ],
            options: CarouselOptions(
              height: MediaQuery.of(context).size.height*0.367,
              autoPlay: true,
              autoPlayAnimationDuration: const Duration(seconds: 4)
            ),
          ),
          
          ],
        ),
      ),
    );
  }
  Container weather(String img){
    return Container(
      height: MediaQuery.sizeOf(context).height*0.200,
      width: MediaQuery.of(context).size.width,
      decoration: const BoxDecoration(
        ),
      child: Image.asset(img),
    );
  }
}