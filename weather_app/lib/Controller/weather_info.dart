import 'package:http/http.dart' as http;
import 'dart:convert';

class WeatherInfo {
  
  
  static String apikey='1337b2ee9c5730aa2643f77771a1b58f';

  //https://api.openweathermap.org/data/2.5/weather?q=Pune&appid=1337b2ee9c5730aa2643f77771a1b58f&units=metric
  static Future<Map<String,dynamic>> getWeather(String city) async{
    Uri url=Uri.parse('https://api.openweathermap.org/data/2.5/weather?q=$city&appid=$apikey&units=metric');

   http.Response response=await http.get(url);
    if(response.statusCode==200){
      var responseData=await jsonDecode(response.body);
      return responseData;
     }
    else{
      throw Exception('Failed to load Weather');
    }
  }


}