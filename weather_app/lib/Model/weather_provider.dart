
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:weather_app/Controller/weather_info.dart';


class WeatherProvider extends ChangeNotifier{

  WeatherInfo weather=WeatherInfo();
  Map<String,dynamic> weatherData={};
  String city='Pune';
  bool isLoading=false;
  String errmsg='Failed to load City';
  List<String> lastCities=[];

  WeatherProvider(Map<String,dynamic> weatherData1,String city1,bool isloading,String errmsg1){
    weatherData=weatherData1;
    city=city1;
    isLoading=isloading;
    errmsg=errmsg1;
    loadlastCity();
  }

 
  //late SharedPreferences prefs;
  

  void getWeather(String city) async {
    isLoading=true;
    errmsg='';
    //notifyListeners();

    try{
      weatherData=await WeatherInfo.getWeather(city);
      city=city;
      isLoading=true;
      notifyListeners();
    }catch(e){
      errmsg=e.toString();
      isLoading=false;
    }
  }

  void setError(String error){
    errmsg=error;
    notifyListeners();
  }

  void saveLastCity(String city)async{
    final prefs=await SharedPreferences.getInstance();
    lastCities.add(city);
    prefs.setStringList('last_cities', lastCities);
  }

  void loadlastCity()async{
    final prefs=await SharedPreferences.getInstance();
    prefs.setStringList('last_cities', lastCities);
    saveLastCity(city);
    
  }

  List getLastCity(){
    
    notifyListeners();
    return lastCities;
  }


}
