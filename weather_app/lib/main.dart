import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:weather_app/Model/weather_provider.dart';
import 'package:weather_app/View/home_screen.dart';

void main() {
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create:(context)=>WeatherProvider({},'',false,''),
      child: const MaterialApp(
        debugShowCheckedModeBanner: false,
        home: HomeScreen()
      ),
    );
  }
}
